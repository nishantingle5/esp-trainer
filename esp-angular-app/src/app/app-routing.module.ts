import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Numbers2dComponent} from './numbers2d/numbers2d.component';
import {StartpageComponent} from './startpage/startpage.component';

const routes: Routes = [
  {
    path: 'numbers2d',
    component: Numbers2dComponent
  },
  {
    path: '',
    component: StartpageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

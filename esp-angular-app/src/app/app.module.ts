import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Numbers2dComponent } from './numbers2d/numbers2d.component';
import { AppRoutingModule } from './app-routing.module';
import { StartpageComponent } from './startpage/startpage.component';
import {HttpClientModule} from '@angular/common/http';
import {AlertModule} from 'ngx-bootstrap/alert';
import {FlexLayoutModule} from '@angular/flex-layout';

@NgModule({
  declarations: [
    AppComponent,
    Numbers2dComponent,
    StartpageComponent
  ],
  imports: [
    AlertModule.forRoot(),
    FlexLayoutModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

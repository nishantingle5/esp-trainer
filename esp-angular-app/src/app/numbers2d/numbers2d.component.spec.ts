import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Numbers2dComponent } from './numbers2d.component';

describe('Numbers2dComponent', () => {
  let component: Numbers2dComponent;
  let fixture: ComponentFixture<Numbers2dComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Numbers2dComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Numbers2dComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

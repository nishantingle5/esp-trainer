export class Numbers2dModelComponent {
  constructor(private _actualValue: number, private _allValues: number[]) {
  }

  get actualValue(): number {
    console.log('correct answer');
    return this._actualValue;
  }

  get allValues(): number[] {
    console.log('options');
    return this._allValues;
  }
}

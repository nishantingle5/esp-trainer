import { ChangeDetectorRef, Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Numbers2dModelComponent} from './numbers2d.model.component';

@Component({
  selector: 'app-numbers2d',
  templateUrl: './numbers2d.component.html',
  styleUrls: ['./numbers2d.component.css']
})
export class Numbers2dComponent implements OnInit {
  private FETCH_NUM_URL = 'http://localhost:9000/esp/nextNum';
  numberToGuess: Numbers2dModelComponent | undefined;
  numberToShow: string | undefined;
  @ViewChild('nextButton') private nextBtn: ElementRef | undefined;
  private answerFound: boolean | undefined;

  constructor(private http: HttpClient, private renderer: Renderer2, private changeDetector: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.initializeProperties();
  }

  initializeProperties(): void {
    this.http.get<Numbers2dModelComponent>(this.FETCH_NUM_URL)
      .subscribe((data: Numbers2dModelComponent) => {
        this.numberToGuess = data;
        this.changeDetector.detectChanges();
        this.renderer.addClass(this.nextBtn?.nativeElement, 'disabled');
      });
    this.numberToShow = '? ?';
    this.answerFound = false;
  }

  showNum($event: MouseEvent): void {
    if (this.answerFound) {
      return;
    }
    const targetElement: HTMLElement = $event.target as HTMLElement;
    targetElement.classList.remove('btn-outline-warning');
    if (+targetElement.innerText !== this.numberToGuess?.actualValue) {
      targetElement.classList.add('btn-danger');
    }
    this.numberToShow = '' + this.numberToGuess?.actualValue;
    const correctAnswer = this.getCorrectButton(targetElement);
    correctAnswer?.classList.remove('btn-outline-warning');
    correctAnswer?.classList.add('btn-success');

    // disable all number buttons & enable next button
    this.answerFound = true;
    this.renderer.removeClass(this.nextBtn?.nativeElement, 'disabled');
  }

  goToNext(): void {
    if (!this.nextBtn?.nativeElement.classList.contains('disabled')) {
      this.initializeProperties();
    }
  }

  getCorrectButton(targetElement: HTMLElement): Element | undefined{
    const btnCount = targetElement.parentElement?.childElementCount;
    const btnList = targetElement.parentElement?.children;

    if (!!btnCount && !!btnList) {
      for (let i = 0; i < btnCount; i += 1) {
        if (+btnList[i].innerHTML === this.numberToGuess?.actualValue) {
          return btnList[i];
        }
      }
    }

    return undefined;
  }
}

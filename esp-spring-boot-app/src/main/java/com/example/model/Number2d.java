package com.example.model;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class Number2d {
    public int actualValue;
    public List<Integer> allValues;
}

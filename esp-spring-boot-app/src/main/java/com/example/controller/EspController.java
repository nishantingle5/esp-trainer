package com.example.controller;

import com.example.model.Number2d;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@CrossOrigin
@Controller
@RequestMapping("/esp")
public class EspController {

    private static final int MIN = 10;
    private static final int BOUND = 100;


    @ResponseBody
    @RequestMapping(value = "/nextNum", produces = MediaType.APPLICATION_JSON_VALUE)
    public Number2d getNextNum() {
        List<Integer> randomIntegers = ThreadLocalRandom.current().ints(MIN, BOUND)
                .distinct().limit(4).boxed().collect(Collectors.toList());
        int randomNumber = randomIntegers.get(
                ThreadLocalRandom.current().ints(0, randomIntegers.size())
                .findFirst().getAsInt()
        );
        Number2d number2d = new Number2d(randomNumber, randomIntegers);
        return number2d;
    }
}